package deloitte.academy.lesson01.entity;

/**
 * 
 * <p> Clase abstracta 'Articulo', encargada de generar los gets & sets de sus respectivos atributos,
 * asi como tambi�n de constructores.
 * </p> 
 * 
 * 
 * @author nnavarrete@externosdeloittemx.com
 * @version 1.0
 * @since 10/03/2020
 */
public abstract class Articulo {

	private String codigo;
	private String nombre;
	private double precio;
	private int cantidad;
	private String fecha;

	public Articulo() {
		// TODO Auto-generated constructor stub
	}

	public Articulo(String codigo, String nombre, double precio, int cantidad, String fecha) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.precio = precio;
		this.cantidad = cantidad;
		this.fecha = fecha;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

}
