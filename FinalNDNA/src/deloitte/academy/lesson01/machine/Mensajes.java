package deloitte.academy.lesson01.machine;
/**
 * <p> Clase de tipo Enum, encargada de mostrar los mensajes de error o de ejecución exitosa,
 * dependiendo del resultado obtenido. </p>
 * 
 * 
 * 
 * @author nnavarrete@externosdeloittemx.com
 * @version 1.0
 * @since 11/03/2020
 */
public enum Mensajes{

	Error("Producto sin existencia"), Listado("Registro General de Productos"), OK("Producto Vendido");
	
	public String descripcion;

	
	private Mensajes(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
    
	
	
	
}
