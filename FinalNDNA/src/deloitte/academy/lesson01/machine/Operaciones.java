package deloitte.academy.lesson01.machine;

import java.util.ArrayList;
import java.util.logging.Logger;
import deloitte.academy.lesson01.entity.Articulo;
import deloitte.academy.lesson01.run.Run;

/**
 * <p>Clase 'Operaciones' encargada de realizar las operaciones de registro, actualizacion, busqueda,
 * eliminaci�n y muestra de ventas realizadas de la maquina dispensadora. </p>
 * 
 * 
 * 
 * @author nnavarrete@externosdeloittemx.com
 * @version 1.0
 * @since 10/03/2020
 * {@link deloitte.academy.lesson01.entity.Articulo} : Clase de la que se extraj� los atributor 
 * utilizados en esta clase.
 */
public class Operaciones extends Articulo {
	
	private static final Logger LOGGER = Logger.getLogger(Operaciones.class.getName());

	
	public Operaciones() {
		// TODO Auto-generated constructor stub
	}

	

	public Operaciones(String codigo, String nombre, double precio, int cantidad, String fecha) {
		super(codigo, nombre, precio, cantidad, fecha);
		// TODO Auto-generated constructor stub
	}



	public Operaciones(Operaciones venta) {
		// TODO Auto-generated constructor stub
	}


	/**
	 * <p>Metodo encargado de agregar productos a la lista existente, llamada 'listaExpendedora'. </p>
	 * @param listaExpendedora 'listaExpendedora' lista que almacena los registros totales en la m�quina expendedora.
	 * @param prod registro a agregar a la lista.
	 * 
	 * 
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 10/03/2020
	 */
	public static void addProd(ArrayList<Operaciones> listaExpendedora, Operaciones prod) {
		listaExpendedora.add(prod);
	}

	/**
	 * <p>Metodo que se encarga de agregar un producto a la lista, verificando antes que el
	 * codigo no se encuentre en la lista. </p>
	 * 
	 * @param : Recibe un registro para agregar a la lista existente.
	 * @return : Regresa un mensaje, mostrando el resultado, verificando si se agreg� o no el registro.
	 * {@code} : (x.getCodigo() != registrop.getCodigo(), compara que no exista el codigo dentro de la
	 * lista, para poder agregarlo.
	 * 
	 *  
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 10/03/2020 
	 *  
	 */
	public String registroProducto(Operaciones registrop) {
		
		
		String mensaje = "";
		try {

			for (Operaciones x : Run.listaExpendedora) {
				if (x.getCodigo() != registrop.getCodigo()) {
					Run.listaExpendedora.add(x);
					mensaje = "Articulo agregado";
					break;
				}

				else {
					mensaje = "Producto ya existente";
					break;
				}

			}

		} catch (Exception ex) {
			System.out.println(Mensajes.Error);
		}

		return mensaje;
	}

	/**
	 * <p>Metodo encargado de eliminar un producto de la lista 'listaExpendedora'. </p>
	 * 
	 * @param 'listaExpendedora' de la cual se tomar� el registro a eliminar.
	 * @param prodructo a eliminar de la lista.
	 * 
	 * 
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 10/03/2020
	 */
	public static void delete(ArrayList<Operaciones> listaExpendedora, Operaciones prod) {
		// TODO Auto-generated method stub
		listaExpendedora.remove(prod);
	}


	/**
	 * <p>Metodo encargado de actualizar el nombre del producto que se debe encontrar dentro
	 * de la lista 'listaExpendedora'. </p>
	 * 
	 * @param listaExpendedora: de la cual se tomar� el registro a actualizar.
	 * @param prod: el registro que se desea actualizar
	 * 
	 * 
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 10/03/2020
	 */
	public void updateNombre(ArrayList<Operaciones> listaExpendedora, String nom) {
		listaExpendedora.get(1).setNombre(nom);
	}

	/**
	 * <p>M�todo que se encarga de actualizar los productos que se encuentren en la
	 * lista. Verificando que dicho producto se encuentre dentro del arreglo.</p>
	 * 
	 * @param Recibe el registro que se desea actualizar. 
	 * @return Regresa un mensaje que mostrar� el resultado obtenido de la acci�n.
	 * @exception Mandar� un mensaje de 'Producto no existente', en caso de que el producto no se 
	 * encuentre dentro de la lista.
	 * 
	 * 
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 10/03/2020
	 * 
	 */
	public String updateProducto(Operaciones registrop) {
		String mensaje = "";
		try {

			registrop = Run.listaExpendedora.get(0);
			for (Operaciones x : Run.listaExpendedora) {
				if (x.getCodigo() == registrop.getCodigo()) {
					mensaje = "Articulo modificado";
					break;
				} else {
					mensaje = "Producto no existente";
					break;
				}

			}

		} catch (Exception ex) {
			LOGGER.info("Error" + Mensajes.Error);
			
		}

		return mensaje;

	}

	/**
	 * <p>M�todo que se encarga de regresar el numero de elementos que se encuentran
	 * dentro de la lista 'listaExpendedora'.</p>
	 * 
	 * @param recibe la lista, para contabilizar los elementos en ella.
	 * @return el numero de enteros encontrados en la lista.
	 * 
	 * 
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 10/03/2020
	 */
	public static int totalLista(ArrayList<Operaciones> listaExpendedora) {
		int lista = listaExpendedora.size();
		listaExpendedora.size();
		return lista;
	}

	/**
	 * <p>M�todo que se encarga de ejecutar la venta de un producto, decrementando su
	 * cantidad en stock. </p>
	 * 
	 * @param listaExpendedora: lista de la cual se obtendr� el producto a vender.
	 * @param ventaProd: producto que se desea vender.
	 * @throws: En caso de que el producto no cuente con la suficiente cantidad en stock, se mandar�
	 * un mensaje, impidiendo la acci�n de compra.
	 * {@code Operaciones.addProd(listaExpendedora, p);} Agrega a la 'listaExpendedora' la nueva 
	 * cantidad generada en el stock, despues de su venta.
	 * 
	 * 
	 * 
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 10/03/2020
	 */
	public static void ventaProd(ArrayList<Operaciones> listaExpendedora, ArrayList<Operaciones> ventaProd,
			Operaciones venta) {

		try {

			if (venta.getCantidad() == 0) {
				LOGGER.info("Producto sin stock" + Mensajes.Error);
			} else {

				int numero = (int) (Math.random() * 12 + 1);

				Operaciones p = new Operaciones(venta);
				p.setCantidad(venta.getCantidad() - 1);
				Operaciones.delete(listaExpendedora, venta);
				venta.setCantidad(venta.getCantidad() - p.getCantidad());

				venta.setFecha(numero + "/03" + "/20");

				Operaciones.addProd(ventaProd, venta);
				Operaciones.addProd(listaExpendedora, p);

				LOGGER.info("OK" + Mensajes.OK);
			}

		} catch (Exception ex) {
			System.out.println(Mensajes.Error);
		}
	}
	

}
