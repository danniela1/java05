package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.logging.Logger;

import deloitte.academy.lesson01.machine.Mensajes;
import deloitte.academy.lesson01.machine.Operaciones;

/**
 * <p> Clase 'Main', encargada de ejecutar los metodos generados en la clase 'Operaciones'. </p>
 * @see deloitte.academy.lesson01.machine.Operaciones
 * 
 * 
 * @author nnavarrete@externosdeloittemx.com
 * @version 1.0
 * @since 10/03/2020
 */
public class Run {

	public static ArrayList<Operaciones> listaExpendedora = new ArrayList<Operaciones>();
	public static ArrayList<Operaciones> ventaTotal = new ArrayList<Operaciones>();

	
	//private static final Logger LOGGER = Logger.getLogger(Run.class.getName());

	public static void main(String[] args) {
		// creacion de registros
		Operaciones a1 = new Operaciones("A1", "Chocolate", 10.50, 10, "02/03/20");
		Operaciones a2 = new Operaciones("A2", "Doritos", 15.50, 4, "05/03/20");
		Operaciones a3 = new Operaciones("A3", "Coca", 22.50, 2, "07/03/20");
		Operaciones a4 = new Operaciones("A4", "Gomitas", 8.75, 6, "08/03/20");
		Operaciones a5 = new Operaciones("A5", "Chips", 30, 10, "089/03/20");
		Operaciones a6 = new Operaciones("A6", "Jugo", 15, 2, "10/03/20");
		Operaciones b1 = new Operaciones("B1", "Galletas", 10, 3, "11/03/20");
		Operaciones b2 = new Operaciones("B2", "Canelitas", 120, 6, "12/03/20");
		Operaciones b3 = new Operaciones("B3", "Halls", 10.10, 10, "11/03/20");
		Operaciones b4 = new Operaciones("B4", "Tarta", 3.14, 10, "10/03/20");
		Operaciones b5 = new Operaciones("B5", "Sabritas", 15.55, 0, "09/03/20");
		Operaciones b6 = new Operaciones("B6", "Chetoos", 12.25, 4, "19/03/20");
		Operaciones c1 = new Operaciones("C1", "Rocaleta", 10, 1, "17/03/20");
		Operaciones c2 = new Operaciones("C2", "Rancherito", 14.75, 6, "21/03/20");
		Operaciones c3 = new Operaciones("C3", "Ruffles", 13.15, 10, "22/03/20");
		Operaciones c4 = new Operaciones("C4", "Pizza fr�a", 22, 9, "11/03/20");

		// a�adiendo registros a la lista
		listaExpendedora.add(a1);
		listaExpendedora.add(a2);
		listaExpendedora.add(a3);
		listaExpendedora.add(a4);
		listaExpendedora.add(a5);
		listaExpendedora.add(a6);
		listaExpendedora.add(b1);
		listaExpendedora.add(b2);
		listaExpendedora.add(b3);
		listaExpendedora.add(b5);

		// Muestra de la lista general MOSTRAR funcion
		System.out.println(Mensajes.Listado);
		for (Operaciones p : listaExpendedora) {
			System.out.println("id: " + p.getCodigo() + " Nombre: " + p.getNombre() + " Costo:" + p.getPrecio()
					+ " cantidad" + p.getCantidad());
		}

		// Ingreso de articulo, AGREGAR
		
		Operaciones producto2 = new Operaciones();
		String b = producto2.registroProducto(a2);
		System.out.println("Registro " + b);
		
		Operaciones producto = new Operaciones();
		producto.registroProducto(b4);
		
		System.out.println(Mensajes.Listado);
		for (Operaciones p : listaExpendedora) {
			System.out.println("id: " + p.getCodigo() + " Nombre: " + p.getNombre() + " Costo:" + p.getPrecio()
					+ " cantidad" + p.getCantidad());
		}
		
		
		Operaciones.ventaProd(listaExpendedora, ventaTotal, b5); //mandar como string en el metodo el codigo

		// MODIFICAR
		a2.updateNombre(listaExpendedora, "cheetos");
		System.out.println(Mensajes.Listado);

		for (Operaciones p : listaExpendedora) {
			System.out.println("id: " + p.getCodigo() + " Nombre: " + p.getNombre() + " Costo:" + p.getPrecio()
					+ "cantidad" + p.getCantidad());
		}
			

		// DELETE/COMPRA de producto
		Operaciones.totalLista(listaExpendedora);
		System.out.println(Operaciones.totalLista(listaExpendedora));
		Operaciones.ventaProd(listaExpendedora, ventaTotal, b2);
		Operaciones.ventaProd(listaExpendedora, ventaTotal, b3);
		Operaciones.ventaProd(listaExpendedora, ventaTotal, a4);
		Operaciones.ventaProd(listaExpendedora, ventaTotal, a2);
		// producto sin existencia
		Operaciones.ventaProd(listaExpendedora, ventaTotal, b5);

		// lista de ventas totales
		System.out.println("ventas totales ");
		for (Operaciones p : ventaTotal) {
			System.out.println("producto: " + p.getNombre() + " cant:" + p.getCantidad());
		}

		
	}

}
